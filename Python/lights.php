<!-- NAO PHP to Python code !-->
<?php include ("naopython.php")?>

<html>
<head>
 <meta charset="UTF-8"/>
 <meta name="description" content="controls">
 <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <!-- Header !-->
    <?php include ("header.php");?>

    <!-- Main Div !-->
    <div class="mainDivPvlt">

        <!-- Left Info Div !-->
        <div class="infoDivPvlt">
            <img src="images/test.png" id="robotImgPvlt">
                <p id="infoParaPvlt">I can switch my LEDs!</p>
        </div>

        <div class="lineDivPvlt"></div>

        <!-- Right Buttons Div !-->
        <div class="contentDivPvlt">
            <h1 id="contentTitlePvlt">Switch LEDs</h1>
            <form action="lights.php" method="post" class="formIntPvlt">
                <input type="submit" class="formBtnPvlt" id="ledsOnJSPvlt" name="ledsOnPvlt" value="Turn LEDs on" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
                <input type="submit" class="formBtnPvlt" id="ledsOffJSPvlt" name="ledsOffPvlt" value="Turn LEDs off" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
            </form>
        </div>
    </div>
</body>

<!-- Image Hover JS !-->
<script type="text/javascript" src="scripts/imghoverpvlt.js"></script>

</html>