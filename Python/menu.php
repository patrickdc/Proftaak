<!-- NAO PHP to Python code !-->
<?php include ("naopython.php")?>

<html>
<head>
 <meta charset="UTF-8"/>
 <meta name="description" content="controls">
 <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <?php include ("header.php");?>
    <div class="mainDivPvlt">
        <div class = "pageDescRelz">Configuration</div>
            <div id="robotMenuPvlt">
                <form action="menu.php" method="post" id="menuFormPvlt">
                    <h1 class="formTitlePvlt">Press the robot's button and write the IP below:</h1>
                        <input type="text" id="formTxtPvlt" name="formIPPvlt" placeholder="<?php echo $ipFieldPvlt; ?>">
                        <br>
                        <button type="submit" id="closeBtnPvlt">Save</button>
                    </form>
        </div>
    </div>
</body>

</html>