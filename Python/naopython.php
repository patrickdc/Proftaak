<?php
    session_start();
    
    //When POST is active
    if($_SERVER['REQUEST_METHOD'] == "POST")
    {
        //If something is entered in the IP field the session will store it
        if (!empty($_POST['formIPPvlt']))
        {
            $_SESSION['sessionIpPvlt'] = $_POST['formIPPvlt'];
        }

        //Get IP from form
        $naoIPPvlt = $_SESSION['sessionIpPvlt'];

        $myfile = fopen("scripts/setIPPvlt.py", "w") or die("Unable to save IP.");
        $txt = "naoIPPvlt = '$naoIPPvlt'";
        fwrite($myfile, $txt);
        
        //Robot scripts
        switch(true)
        {
            case isset($_POST['dialogOnePvlt']):
                exec ("python scripts/dialogonepvlt.py");
                    break;
            case isset($_POST['dialogTwoPvlt']):
                exec ("python scripts/dialogtwopvlt.py");   
                    break;
            case isset($_POST['dialogThreePvlt']):
                exec ("python scripts/dialogthreepvlt.py");   
                    break;
            case isset($_POST['poseCrouchPvlt']):
                exec("python scripts/posecrouchpvlt.py");  
                    break;
            case isset($_POST['poseLiePvlt']):
                exec ("python scripts/poseliepvlt.py");
                    break;
            case isset($_POST['poseSitPvlt']):
                exec ("python scripts/posesitpvlt.py");
                    break;
            case isset($_POST['poseStandPvlt']):
                exec ("python scripts/posestandpvlt.py");
                    break;
        }
    }

    //Clears field if no IP has been entered, or fills it if known
    if(empty($_SESSION['sessionIpPvlt']))
    {
        $ipFieldPvlt="";
    }
    else
    {
        $ipFieldPvlt=$_SESSION['sessionIpPvlt'];
    }
?>