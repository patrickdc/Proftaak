<!-- NAO PHP to Python code !-->
<?php include ("naopython.php")?>

<html>
<head>
 <meta charset="UTF-8"/>
 <meta name="description" content="controls">
 <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <!-- Header !-->
    <?php include ("header.php");?>

    <!-- Main Div !-->
    <div class="mainDivPvlt">
        
        <!-- Left Info Div !-->
        <div class="infoDivPvlt">
            <img src="images/test.png" id="robotImgPvlt">
                <p id="infoParaPvlt">I can pose for pictures!</p>
        </div>

        <div class="lineDivPvlt"></div>

        <!-- Right Buttons Div !-->
        <div class="contentDivPvlt">
            <h1 id="contentTitlePvlt">What should I do?</h1>
            <form action="poses.php" method="post" class="formIntPvlt">
                <input type="submit" class="formBtnPvlt" id="poseCrouchJSPvlt" name="poseCrouchPvlt" value="Crouch" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
                <input type="submit" class="formBtnPvlt" id="poseLieJSPvlt" name="poseLiePvlt" value="Lie" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
                <input type="submit" class="formBtnPvlt" id="poseSitJSPvlt" name="poseSitPvlt" value="Sit" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
                <input type="submit" class="formBtnPvlt" id="poseStandJSPvlt" name="poseStandPvlt" value="Stand" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
                <br>
            </form>
        </div>
    </div>
</body>

<!-- Image Hover JS !-->
<script type="text/javascript" src="scripts/imghoverpvlt.js"></script>

</html>