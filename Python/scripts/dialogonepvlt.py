import socket
from naoqi import ALProxy

import setIPPvlt
naoIPPvlt = setIPPvlt.naoIPPvlt

try:
    tts = ALProxy("ALTextToSpeech", naoIPPvlt, 9559)
    tts.setLanguage("English")
    tts.say("Hello, world!")
    tts.setLanguage("Dutch")
except socket.herror:
    print "Unknown host"
