import socket
from naoqi import ALProxy
import setIPPvlt
naoIPPvlt = setIPPvlt.naoIPPvlt

try:
    tts = ALProxy("ALTextToSpeech", naoIPPvlt, 9559)
    tts.setLanguage("English")
    tts.say("Hi! I'm Eddy, nice to meet you!")
    tts.setLanguage("Dutch")
except socket.herror:
    print "Unknown host"
