let robotImgPvlt = document.getElementById("robotImgPvlt");
let infoTextPvlt = document.getElementById("infoParaPvlt").innerHTML;

    function ChangeImgPvlt(btnInputPvlt)
    {
        switch(btnInputPvlt)
        {
            case 'ledsOnJSPvlt':
                robotImgPvlt.src=("images/rbledson.png");
                document.getElementById("infoParaPvlt").innerHTML = "Turn my LEDs on!";
            break;
            case 'ledsOffJSPvlt':
                robotImgPvlt.src=("images/rbledsoff.png");
                document.getElementById("infoParaPvlt").innerHTML = "Turn my LEDs off";
            break;
            case 'dialogOneJSPvlt':
                robotImgPvlt.src=("images/rbdialogone.png");
                document.getElementById("infoParaPvlt").innerHTML = "Hello World";
            break;
            case 'dialogTwoJSPvlt':
                robotImgPvlt.src=("images/rbdialogtwo.png");
                document.getElementById("infoParaPvlt").innerHTML = "Hi! I'm Eddy, nice to meet you!";
            break;
            case 'dialogThreeJSPvlt':
                robotImgPvlt.src=("images/rbdialogthree.png");
                document.getElementById("infoParaPvlt").innerHTML = "Can I have this dance?";
            break;
            case 'poseCrouchJSPvlt':
                robotImgPvlt.src=("images/rbposecrouch.png");
                document.getElementById("infoParaPvlt").innerHTML = "I will crouch";
            break;
            case 'poseLieJSPvlt':
                robotImgPvlt.src=("images/rbposelie.png");
                document.getElementById("infoParaPvlt").innerHTML = "I'll lie down and rest";
            break;
            case 'poseSitJSPvlt':
                robotImgPvlt.src=("images/rbposesit.png");
                document.getElementById("infoParaPvlt").innerHTML = "I can sit aswell. Easy!";
            break;
            case 'poseStandJSPvlt':
                robotImgPvlt.src=("images/rbposestand.png");
                document.getElementById("infoParaPvlt").innerHTML = "I'll stand up!";
            break;
        }
    }

    function ChangeImgBackPvlt()
    {
        robotImgPvlt.src=("images/test.png");
        document.getElementById("infoParaPvlt").innerHTML = infoTextPvlt;
    }