<!-- NAO PHP to Python code !-->
<?php include ("naopython.php")?>

<html>
<head>
 <meta charset="UTF-8"/>
 <meta name="description" content="controls">
 <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <!-- Header !-->
    <?php include ("header.php");?>

    <!-- Main Div !-->
    <div class="mainDivPvlt">

        <!-- Left Info Div !-->
        <div class="infoDivPvlt">
            <img src="images/test.png" id="robotImgPvlt">
                <p id="infoParaPvlt">I'm able to speak too!</p>
        </div>

        <div class="lineDivPvlt"></div>

        <!-- Right Buttons Div !-->
        <div class="contentDivPvlt">
            <h1 id="contentTitlePvlt">What should I say?</h1>
            <form action="speech.php" method="post" class="formIntPvlt">
                <input type="submit" class="formBtnPvlt" id="dialogOneJSPvlt" name="dialogOnePvlt" value="Hello world!" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
                <input type="submit" class="formBtnPvlt" id="dialogTwoJSPvlt" name="dialogTwoPvlt" value="Introduce" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
                <input type="submit" class="formBtnPvlt" id="dialogThreeJSPvlt" name="dialogThreePvlt" value="Ask for dance" onclick="ChangeImgPvlt(this.id)" onmouseover="ChangeImgPvlt(this.id)" onmouseout="ChangeImgBackPvlt()"/>
            </form>
        </div>
    </div>
</body>

<!-- Image Hover JS !-->
<script type="text/javascript" src="scripts/imghoverpvlt.js"></script>

</html>